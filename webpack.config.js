const path = require('path');
const fs = require('fs')
const CopyWebpackPlugin = require('copy-webpack-plugin')

// console.debug(process.versions)
const declarationDir = "./declarations"
let declarations = fs.readdirSync(declarationDir).filter(file => file.match(/.*\.ts$/))
console.debug('found declarations  : ', declarations)
declarations = declarations.map(d => path.resolve(declarationDir, d))


module.exports = {
    mode: "development",
    // mode: "production",
    entry: {
        bomc: declarations,
        // backend : './backend/index.ts',
        frontend: ['./frontend/app.js', './frontend/index.html' ],

    },
    devtool: 'inline-source-map',
    module: {
        rules: [{
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: "html-loader"
            },
            {
                test: /\.css$/,
                loader: "css-loader"
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    plugins: [
        new CopyWebpackPlugin([
            { from: './frontend/public' },
            { from: './frontend/index.html' },
            { from: './frontend/style.css' },
        ])
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        host: '0.0.0.0',
        port: 9000
      }
};
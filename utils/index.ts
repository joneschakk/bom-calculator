
export function toGB(bytes){
    return bytes /1024 /1024 /1024
}

export const isValue = (val) => val != undefined && val != null

// TODO  make typeof
export const isList = (val) => val && val.length != undefined

export function deepCopy(src) {
    let target = Array.isArray(src) ? [] : {};
    for (let prop in src) {
      let value = src[prop];
      if(value && typeof value === 'object') {
        target[prop] = deepCopy(value);
    } else {
        target[prop] = value;
    }
   }
      return target as any;
  }



export function NumericalRangeValidator(min: number, max: number) {
    return (value: number) => {
        if ((min || min ==0) && value < min)
            return new ValidationError(`should be greater than ${min}`)
        if ((max || max ==0) && value > max)
            return new ValidationError(`should be lees than ${max}`)
   
    }
}

export function StringLengthValidator(min: number, max: number) {
    return (value: string) => {
        if ((min || min ==0) && value.length < min)
            return new ValidationError(`input is too small`)
        if ((max || max ==0) && value.length > max)
            return new ValidationError(`should be less than ${max} chars`)
    }
}
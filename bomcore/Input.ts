import { isValue, isList, deepCopy } from "../utils";
import { StringLengthValidator, NumericalRangeValidator } from "./validation";


interface InputProps {
    id: string,
    description: string,
    type: 'number' | 'string' | 'list' | 'option' | 'bool',
    required?:boolean,
    default?: any,
    min?: number,
    max?: number,
    group: string,
    item?: string,
    items?: string[],
    validator?: Function | null,
}

export interface InputRegProps extends InputProps {
    target: any,
    value?: any,
    model?:any,
    validators: Record<any, Function>
    trasformer?:Function
}

export const RegisteredInputs: Map<string, InputRegProps> = new Map

export function Input(props: InputProps) {
    return (target, key, descriptor) => {
        let validator = props.validator
        let trasformer
        if (typeof (descriptor.value) == "function") {
            trasformer = descriptor.value
        }

        const canBeMultiple = ['list', 'option'].includes(props.type)

        if (canBeMultiple) {
            if (!props.item && !(props.items && props.items.length > 1)) {
                throw "for list/option item(s) should be given"
            }
        }

        let reg: InputRegProps

        if (RegisteredInputs.has(props.id)) {
            // same id already exists. look for list
            reg = RegisteredInputs.get(props.id)

            if (reg.type == props.type && canBeMultiple) {
                if (reg.group == props.group || reg.default == props.default) { // TODO more check
                    // since this input registration also goes on same bucket.
                    console.debug(`input ${reg.id} had another occurance. adding to same bucket`)
                }
                else
                    throw `some props of input ${props.id} collides with ${reg}`
            } else
                throw `cannot redefine input ${props.id}. same id already registered with ${reg}`
        } else {
            reg = { ...props, trasformer, target, validators: {} }
            RegisteredInputs.set(reg.id, reg)
        }

        const passedValidator = props.validator || undefined
        delete reg.validator
        switch (props.type) {
            case 'list':
                let items = []
                isValue(props.item) && items.push(props.item)
                isList(props.items) && props.items.forEach(i => items.push(i))

                if (!reg.items)
                    reg.items = []
                items.forEach(i => {
                    reg.items.push(i)
                    reg.validators[i] = passedValidator
                })
                if (!reg.default)
                    reg.default = []
                props.default && items.forEach(i => reg.default.push(i))
                delete reg.item
                break;

            case 'string':
                reg.validators[0] = passedValidator
                reg.validators['$strlen'] = StringLengthValidator(props.min, props.max)
                break

            case 'number':
                reg.validators[0] = passedValidator
                reg.validators['$numrange'] = NumericalRangeValidator(props.min, props.max)
                break

            case 'bool':
                reg.validators[0] = passedValidator
                break;

            case 'option':
                throw "Not implemeneted";

            default:
                throw `Not implemeneted option: ${props.type}`;
        }
    }
}



export function GetGroupedInputSchema() {
    const grouped: Record<string, InputRegProps[]> = {};
    RegisteredInputs.forEach(i => {
        if (!grouped[i.group])
            grouped[i.group] = []
        const input = deepCopy(i)
        grouped[i.group].push(input)
    })
    return grouped
}
interface ServiceProps {
    id?: string
    realm?: string,
    nodes?: string,
    requiredInputs?: string[]
}

interface ServiceRegProps extends ServiceProps {
    service: any
}

export const RegisteredServices: Map<string, ServiceRegProps> = new Map

export function Service(props: ServiceProps) {
    return (target) => {
        if (!props.id)
            props.id = target.name

        if (RegisteredServices.has(props.id))
            throw `multiple serivices with id ${props.id}`

        const service = new target
        const reg: ServiceRegProps = { ...props, service }
        Object.defineProperty(service,
            "$reg",
            { value: reg, writable: false })
        console.debug(`registering service ${reg}`)
        RegisteredServices.set(reg.id, reg)
    }
}

interface FeatureProps {
    id?: string
    realm?: string,
    nodes?: string,
    requiredInputs?: string[]
}

interface FeatureRegProps extends FeatureProps {
    feature: any
}

export const RegisteredFeatures: Map<string, FeatureRegProps> = new Map

export function Feature(props) {
    return (target) => {
        if (!props.id)
            props.id = target.name

        if (RegisteredFeatures.has(props.id))
            throw `multiple features with id ${props.id}`

        const feature = new target
        const reg: FeatureRegProps = { ...props, feature }
        Object.defineProperty(feature,
            "$reg",
            { value: reg, writable: false })
        console.debug(`registering feature ${reg}`)
        RegisteredFeatures.set(reg.id, reg)
    }
}

import { Input, RegisteredInputs, GetGroupedInputSchema, InputRegProps } from "./Input";
import { Service, RegisteredServices } from "./Service";
import { Feature, RegisteredFeatures } from "./Feature";
import { strict } from "assert";
import { stringify } from "querystring";


// In the browser 'this' is a reference to the global object, called 'window'.
// In Node 'this' is a reference to the module.exports object.

const isBrowser = window && window.alert

export const $core = {
    RegisteredInputs,
    RegisteredServices,
    RegisteredFeatures,
    listInputScheamaGroups  : ()=>{
        let groups : {name:string, inputs: InputRegProps[]}[] = []
        const g_schema = GetGroupedInputSchema()
        for(let name in g_schema){
            groups.push({name, inputs:g_schema[name] })
        }
        return groups
    }
}


if (isBrowser){
    window['$bomcore'] =  $core
}


export {Input, Service, Feature}
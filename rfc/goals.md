Project workflow
==========================
state of project: poc|prod

micro bom
generate result to feed autoinstaller
licence/purchased-feature based bom generation

users
=============
sign in using gmail


rules
-------
    | users         | projects |  view | edit  | admin |
    |  *.flytxt.com |       *  |   y   |   -   |   -   |
    |  abcd@flyxt   |       *  |   y   |   y   |   y   |

anyone in flytxt domain will have read access by above role
previleges
project aproval is needed for finalizing the BOM

project will have multiple version of BoMs

Project
    - id
    - name
    - stage
  
Version
    - id
    - projectId
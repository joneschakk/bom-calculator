import { Feature, Input } from "../bomcore";


@Feature({})
class RoboX {

    @Input({
        id: "mlModels",
        description: "models",
        type: "list",
        default: true, //selects by default
        item: 'Robo-X',
        group: "Machine lerning"
    })
    validateRoboXChoise(value) {}

    cpu() {
        return 0
    }

    memory() {
        return 0
    }

    hdfsSize() {
        return 0
    }
}


@Feature({})
class Abcd {

    @Input({
        id: "mlModels",
        description: "models",
        type: "list",
        default: true, //selects by default
        item: 'Abcd',
        group: "Machine lerning"
    })
    validateRoboXChoise(value) {}

    cpu() {
        return 0
    }

    memory() {
        return 0
    }

    hdfsSize() {
        return 0
    }
}
import { Input } from "../bomcore";

class Inputs {
    @Input({
        id: "noBC",
        description: "No. of Targeted BCs per day",
        type: 'number',
        default: 30,
        required: true,
        min: 10,
        max: 99999,
        group: "Big Data",
        validator: null
    })
    noBcInput(value,ctx) {
        
     }

    @Input({
        id: "noRealTimeBC",
        description: "No. of Targeted BCs per day",
        required: true,
        type: 'number',
        default: 30,
        min: 10,
        max: 99999,
        group: "Big Data",
        validator: null
    })
    noRealTimeBCInput(value, ctx) { 

    }

    @Input({
        id: "domainName",
        description: "Domain name",
        type: 'string',
        default: '',
        min: 3,
        max: 30,
        group: "Installation"
    })
    domainnameInput(value, ctx) {

    }
}

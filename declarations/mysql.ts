import { toGB } from "../utils";
import { Service, Input } from "../bomcore";

@Service({
    realm: 'system',
    requiredInputs: ["noBC", "noRealTimeBC"]
})
class Mysql{

    isRequired() {
        return true
    }

    cpu() {
        return this.memory() * 0.5
    }

    memory() {
        return this.hdd() * 0.75
    }

    hdd() {
        const { noBC, noRealTimeBC, } = this.ctx.input

        // follwing calculations yeild size in bytes
        const appInstance = 8754 * (noBC + noRealTimeBC) * 120;
        const target = 9379 * (noBC + noRealTimeBC) * 120;
        const schData = 20414 * (noBC.value + noRealTimeBC) * 120;
        const transportPerfomance = 101 * (noBC + noRealTimeBC) * 120;

        const othersGb = 12;
        return toGB(appInstance + target + schData + transportPerfomance) + othersGb
    }
}

let m= new Mysql()
console.log(m.ctx, (Mysql as any).ctx);

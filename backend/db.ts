import "reflect-metadata";
import {createConnection, getConnectionOptions, ConnectionOptionsReader, Connection} from "typeorm";
import {Project} from "./entity/Project";


let DEV_MODE=false

if (process.env.BOMC_DEV === "true" ){
    console.log("! application running on dev mode");
    DEV_MODE = true
}

export async  function InitDatabase(){
    createNewDbConnection()
}

async function createNewDbConnection(){
    let  configName = 'ormconfig'
    if (DEV_MODE == true)
        configName = 'ormconfig.dev'
    
    const confReader = new ConnectionOptionsReader({configName})
    const connectionOptions = await confReader.get("default")
    const connection =  await createConnection(connectionOptions)
    return connection
}


let conn :Connection
export async function GetDBConnection(){
    if(conn && conn.isConnected)
        return conn
    else {
        console.log("creating new connection to db")
        conn = await createNewDbConnection()
        return conn
    }
}
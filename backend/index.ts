import "reflect-metadata";
 
import {InitDatabase} from './db'
import * as Koa from 'koa'
import * as Router from 'koa-router'
import {AppRoutes} from './routes'
import * as bodyParser from "koa-bodyparser";


async function main(){
    const app = new Koa()
    const router = new Router()
    console.log("inititalizing database..")
    await InitDatabase()
    
    AppRoutes.forEach(route => router[route.method](route.path, route.action));

    app.use(bodyParser({   
        enableTypes:['json','text'],
        extendTypes: {
            json: ['text'] }
    }));
    app.use(router.routes());
    app.use(router.allowedMethods());
    
    app.listen(3000, '0.0.0.0');
    console.log(`application is up at http://0.0.0.0:3000`);
}

main().catch(
    (reason)=>{
        console.log("prgram crashed")
        console.error(reason)
    }
)
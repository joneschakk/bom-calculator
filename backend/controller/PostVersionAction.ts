import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Version} from "../entity/Version";

export default async function PostVersionAction(ctx: Context) {

    // get a post repository to perform operations with post
    const postRepository = getManager().getRepository(Version);
    // console.log('body',ctx.request.rawBody, ctx.request.length)
    // load all posts
    const body = ctx.request.body
    if (body.appVersion.length==0){
        ctx.response.status = 405
        ctx.body= {"error":"AppVersion missing"}
    }
    const version = await postRepository.find({where:{appVersion:Equal(ctx.params.vid),projectId: Equal(ctx.params.id)}});
    
    
    
    if (version.length == 0 )
    {
        const versions = await postRepository.create(ctx.request.body);
        await postRepository.save(versions);
        console.log("Successfully added version",body);
        ctx.body = body;
    }
    else{
        ctx.body = {"error":"Version already exists"}
        ctx.response.status = 409
    }
    
}

import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Version} from "../entity/Version";

export default async function GetVersionsAction(ctx: Context) {
    const versionRepository = getManager().getRepository(Version);

    // load all posts
    const versions = await versionRepository.find({where:{projectId: Equal(ctx.params.id)}});
    if (versions.length==0){
        ctx.response.status =404
    }
    else
    {
        console.log("Found the version", versions)
        ctx.body = versions;
    }
    // return loaded posts
    
}
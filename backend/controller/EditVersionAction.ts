import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Version} from "../entity/Version";


export default async function EditVersionAction(ctx: Context) {

    // get a post repository to perform operations with post
    const postRepository = getManager().getRepository(Version);
    // console.log('body',ctx.request.rawBody, ctx.request.length)
    // load all posts

    const version = await postRepository.find({where:{appVersion:Equal(ctx.params.vid),projectId: Equal(ctx.params.id)}});
    const body = ctx.request.body
    
    console.log(version)
    
    
    
    if (version.length!=0 ) {
        
        
        const upd = await postRepository.update({appVersion:ctx.params.vid,projectId:ctx.params.id},body);  //updates where id=params.id and appVersion=params.vid --- body is the json list of parameters to be updated
        console.log("Successfully updated version",upd);
        ctx.body = body;
    }
    else{

        ctx.body = {"error":"Version doesnt exist"}
        ctx.response.status = 404
        
    }
    
}

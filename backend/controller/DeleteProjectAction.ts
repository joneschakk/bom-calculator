import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Project} from "../entity/Project";

export default async function PostProjectAction(ctx: Context) {

    // get a post repository to perform operations with post
    const postRepository = getManager().getRepository(Project);
    // console.log('body',ctx.request.rawBody, ctx.request.length)
    // load all posts

    const project = await postRepository.find({name: Equal(ctx.params.id)});
    const body = ctx.request.body
    
    
    if (project)
    {
        await postRepository.remove(project);
        
        console.log("Successfully removed",body)
        ctx.body = body;
    }
    else{
        ctx.body = "Project doesnt exists"
        ctx.response.status = 404
    }

    
    // return loaded posts
    
    
}
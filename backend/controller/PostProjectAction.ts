import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Project} from "../entity/Project";

export default async function PostProjectAction(ctx: Context) {

    // get a post repository to perform operations with post
    const postRepository = getManager().getRepository(Project);
    // console.log('body',ctx.request.rawBody, ctx.request.length)
    // load all posts
    const body = ctx.request.body
    const project = await postRepository.findOne({name: Equal(body.name)})
    
    
    
    if (!project)
    {
        const projects = await postRepository.create(ctx.request.body);
        await postRepository.save(projects);
        console.log("Successfully added",body)
        ctx.body = body;
    }
    else{
        ctx.body = "Project already exists"
        ctx.response.status = 409
    }

    
    // return loaded posts
    
    
}
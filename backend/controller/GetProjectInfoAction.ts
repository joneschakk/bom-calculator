import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Project} from "../entity/Project";

export default async function GetProjectInfoAction(ctx: Context) {
    const projectRepository = getManager().getRepository(Project);

    // load all posts
    const project = await projectRepository.findOne({where:{id:ctx.params.id},relations:["versions"]});
    if (!project){
        ctx.response.status =404
    }
    // return loaded posts
    else{
        ctx.body = project;
    }
}
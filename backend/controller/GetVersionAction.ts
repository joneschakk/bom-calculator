import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Version} from "../entity/Version";

export default async function GetVersionAction(ctx: Context) {
    const versionRepository = getManager().getRepository(Version);

    // load all posts
    const version = await versionRepository.find({where:{appVersion:Equal(ctx.params.vid),projectId: Equal(ctx.params.id)}});
    if (version.length==0){
        ctx.response.status =404
    }
    else
    {
        console.log("Found the version", version)
        ctx.body = version;
    }
    // return loaded posts
    
}
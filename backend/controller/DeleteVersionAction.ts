import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Version} from "../entity/Version";

export default async function PostProjectAction(ctx: Context) {

    // get a post repository to perform operations with post
    const postRepository = getManager().getRepository(Version);
    // console.log('body',ctx.request.rawBody, ctx.request.length)
    // load all posts

    const version = await postRepository.find({appVersion: Equal(ctx.params.vid),projectId: Equal(ctx.params.id)});
    const body = ctx.request.body
    
    
    if (version)
    {
        await postRepository.remove(version);
        
        console.log("Successfully removed",body)
        ctx.body = body;
    }
    else{
        ctx.body = "Version doesnt exists"
        ctx.response.status = 404
    }

    
    // return loaded posts
    
    
}
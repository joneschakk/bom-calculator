import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Project} from "../entity/Project";

export default async function GetProjectsInfoAction(ctx: Context) {
    const projectRepository = getManager().getRepository(Project);

    // load all posts
    const projects = await projectRepository.find();
    if (!projects){
        ctx.response.status =404
    }
    // return loaded posts
    else{
        ctx.body = projects;
    }
}
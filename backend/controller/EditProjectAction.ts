import {Context} from "koa";
import {getManager,Equal} from "typeorm";
import {Project} from "../entity/Project";

export default async function EditProjectsAction(ctx: Context) {

    // get a post repository to perform operations with post
    const projectRepository = getManager().getRepository(Project);
    // console.log('body',ctx.request.rawBody, ctx.request.length)
    // load all posts

    const project = await projectRepository.findOne({where:{id: Equal(ctx.params.id)}});
    const body = ctx.request.body
    
    
    if (!project){

        ctx.body = "Project doesnt exists"
        ctx.response.status = 404
        
    }
    else{
        
        const project_updt = await projectRepository.create(ctx.request.body);
        //await projectRepository.remove(project);
        console.log(body)
        await projectRepository.update(ctx.params.id,body);   //updates where id=params.id and body is the json list of parameters to be updated
        
        console.log("Successfully updated project",body);
        ctx.body = body;
    }
}


import GetProjectInfoAction from "./controller/GetProjectInfoAction";
import PostProjectAction from "./controller/PostProjectAction";
import EditProjectAction from "./controller/EditProjectAction";
import DeleteProjectAction from "./controller/DeleteProjectAction";
import GetProjectsAction from "./controller/GetProjectsAction";

import PostVersionAction from "./controller/PostVersionAction";
import GetVersionAction from "./controller/GetVersionAction";
import GetVersionsAction from "./controller/GetVersionsAction";
import EditVersionAction from "./controller/EditVersionAction";
import DeleteVersionAction from "./controller/DeleteVersionAction";
/**
 * All application routes.
 */
export const AppRoutes  = [
    {
        path: "/projects/:id",
        method: "get",
        action: GetProjectInfoAction
    },
    {
        path: "/projects/",
        method: "get",
        action: GetProjectsAction
    },
    {
        path: "/projects",
        method: "post",
        action: PostProjectAction
    },
    {
        path: "/projects/:id",
        method: "patch",
        action: EditProjectAction
    },
    {
        path: "/projects/:id",
        method: "delete",
        action: DeleteProjectAction
    },
    

    // VERSION
    {
        path: "/projects/:id/versions/:vid",
        method: "get",
        action: GetVersionAction
    },
    {
        path: "/projects/:id/versions",
        method: "get",
        action: GetVersionsAction
    },
    {
        path: "/projects/:id/versions",
        method: "post",
        action: PostVersionAction
    },
   
    {
        path: "/projects/:id/versions/:vid",
        method: "patch",
        action: EditVersionAction
    },
    {
        path: "/projects/:id/versions/:vid",
        method: "delete",
        action: DeleteVersionAction
    }
];
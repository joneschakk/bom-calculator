import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {Project} from "./Project"
import { type } from "os";

@Entity()
export class Version {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    appVersion: string;

    @Column()
    createdUser: string;

    @Column()
    createdAt: Date;
    
    @Column()
    modifiedUser: string;
    
    @Column()
    modifiedAt: Date;

    @Column()
    input: String;

    @ManyToOne(type=> Project, project=> project.versions,{onDelete:"CASCADE",cascade:["update"]})
    projectId: Project;
}

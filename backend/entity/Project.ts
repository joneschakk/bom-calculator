import {Entity, PrimaryGeneratedColumn, Column,OneToMany} from "typeorm";
import { Version } from "./Version";


@Entity()
export class Project {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({nullable:true})
    state: string;

    @Column()
    createdUser: string;

    @Column()
    createdAt: Date;

    @OneToMany(type=>Version, version=> version.projectId)
    versions: Version[];
}
